<?php
 
    class Banner
    {
        //Definindo atributos privados
        private $id;
        private $titulo;
        private $link;
        private $img;
        private $alt;
        private $ativo;
        

        //Método de acesso para o ID get e set
        public function getId()
        {
            return $this->id;
        }
        public function setId($value)
        {
            $this->id = $value;
        }
        //Método de acesso para o Titulo get e set
        public function getTitulo()
        {
            return $this->titulo;
        }
        public function setTitulo($value)
        {
            $this->titulo = $value;
        }
        //Método de acesso para o Link get e set
        public function getLink()
        {
            return $this->link;
        }
        public function setLink($value)
        {
            $this->link = $value;
        }
        //Método de acesso para o Img get e set
        public function getImg()
        {
            return $this->img;
        }
        public function setImg($value)
        {
            $this->img = $value;
        }
        //Método de acesso para o Alt get e set
        public function getAlt()
        {
            return $this->alt;
        }
        public function setAlt($value)
        {
            $this->alt = $value;
        }
        //Método de acesso para o Ativo get e set
        public function getAtivo()
        {
            return $this->ativo;
        }
        public function setAtivo($value)
        {
            $this->ativo = $value;
        }
       

        //Pesquisando banner pelo ID
        public function pesquisaId($_id)
        {
            $sql = new Sql();
            return $sql->select('select * from banner where id_banner = :id',array(':id'=>$_id));
        }
        //Inserindo banner
        public function insertBanner($_titulo,$_link,$_img,$_alt,$_ativo)
        {
            $sql = new Sql();
            $results = $sql->select('CALL sp_banner_insert(:titulo,:link,:img,:alt,:ativo)',
            array(
                ':titulo'=>$_titulo,
                ':link'=>$_link,
                ':img'=>$_img,
                ':alt'=>$_alt,
                ':ativo'=>$_ativo));

            if (count($results) > 0) 
            {
                $this->setData($results[0]);
            }              
        }
        //Excluindo banner
        public function deleteBanner($_id)
        {
            $sql = new Sql();
            $sql->query('DELETE from banner where id_banner = :id',array(':id'=>$_id));
        }
        //Alterando banner
        public function updateBanner($_id,$_titulo,$_link,$_img,$_alt,$_ativo)
        {
            $sql = new Sql();
            $sql->query('UPDATE banner set titulo_banner = :titulo,link_banner = :link, img_banner = :img,alt=:alt,banner_ativo=:ativo where id_banner = :id',
            array(
                ':titulo'=>$_titulo,
                ':link'=>$_link,
                ':img'=>$_img,
                ':alt'=>$_alt,
                ':ativo'=>$_ativo,
                ':id'=>$_id));
        }
        //Listando banner
        public function listarBanner()
        {
            $sql = new Sql();
            return $sql->select('SELECT * FROM banner');
        }
        //Consultar banner
        public function pesquisaTitulo($_titulo)
        {
            $sql = new Sql();
            return $sql->select('SELECT * FROM banner where titulo_banner LIKE :titulo',array(':titulo'=>'%'.$_titulo.'%'));
        }
        //Método construtor da classe
        public function __construct($_id='',$_titulo='',$_link='',$_img='',$_alt='',$_ativo='')
        {
            $this->id = $_id;
            $this->titulo = $_titulo;
            $this->link = $_link;
            $this->img = $_img;
            $this->alt = $_alt;
            $this->ativo = $_ativo;
        }
        //Define os atributos 
        public function setData($data)
        {
            $this->setId($data['id_banner']);
            $this->setTitulo($data['titulo_banner']);
            $this->setLink($data['link_banner']);
            $this->setImg($data['img_banner']);
            $this->setAlt($data['alt']);
            $this->setAtivo($data['banner_ativo']);
        }
    }
?>