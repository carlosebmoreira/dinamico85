<?php  
    require_once('../config.php');
    //Inserindo banner
    if(isset($_POST['btn_cadastrar']))
    {         
        $ban = new Banner();
        $img = upload_imagem();
        if(isset($img[0]))
        {
            $ban->insertBanner($_POST['txt_titulo'],$_POST['txt_link'],$img[0],$_POST['txt_alt'],isset($_POST['check_ativo'])? '1':'0');
            if($ban->getId()>0)        
            {
                header('location:principal.php?link=8&msg=ok');
            }
            else
            {
                header('location:principal.php?link=8');
            }        
        }
    }
    //Deletando banner
    if($_GET['delete']==1 && isset($_GET['id']))
    {
        $ban = new Banner();
        $ban->deleteBanner($_GET['id']);        
        header('location:principal.php?link=9');
    }
    //Update banner
    if($_GET['update']==1 && isset($_POST['id']))
    {
        $ban = new Banner();
        $foto = $_FILES['img'];
        if(isset($_FILES['img']) && !empty($foto['name']))
        {
            $img = upload_imagem();
            $ban->updateBanner($_POST['id'],$_POST['titulo'],$_POST['link'],$img[0],$_POST['alt'],isset($_POST['ativo'])?'s':'n');
            header('location:principal.php?link=9');
        }
        else if(isset($_POST['img_atual']))
        {
            $ban->updateBanner($_POST['id'],$_POST['titulo'],$_POST['link'],$_POST['img_atual'],$_POST['alt'],isset($_POST['ativo'])?'s':'n');
            header('location:principal.php?link=9');
        }        
    }
?>