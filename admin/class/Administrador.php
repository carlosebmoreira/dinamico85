<?php

    class Administrador
    {
        //Declaração de atributos Privates
        private $id;
        private $nome;
        private $email;
        private $login;
        private $senha;

        //Método de acesso para ID
        public function getId()
        {
            return $this->id;
        }
        public function setId($value)
        {
            $this->id = $value;
        }
        //Método de acesso para Nome
        public function getNome()
        {
            return $this->nome;
        }
        public function setNome($value)
        {
            $this->nome = $value;
        }
        //Método de acesso para Email
        public function getEmail()
        {
            return $this->email;
        }
        public function setEmail($value)
        {
            $this->email = $value;
        }
        //Método de acesso para Login
        public function getLogin()
        {
            return $this->login;
        }
        public function setLogin($value)
        {
            $this->login = $value;
        }
        //Método de acesso para Senha
        public function getSenha()
        {
            return $this->senha;
        }
        public function setSenha($value)
        {
            $this->senha = $value;
        }
        //Retorna o administrador do banco pelo id
        public function loadById($_id)
        {
            $sql = new Sql();
            $results = $sql->select('SELECT * FROM administrador where id = :id', array(':id' => $_id));
            if (cont($results) > 0) 
            {
                $this->setData($results[0]);
            }
        }
        //Retorna uma lista de administrador
        public static function getList()
        {
            $sql = new Sql();
            return $sql->select('SELECT * FROM administrador order by nome');
        }
        //Este metodo retorna uma pesquisa pelo nome da pessoa onde há pelo menos 2 letras que o usuario digitou
        public static function search($nome_adm)
        {
            $sql = new Sql();
            return $sql->select('SELECT * FROM administrador where nome LIKE :nome',array(":nome"=>"%".$nome_adm."%"));
        }
        //Este método retorna os dados do usuario que digitar seu login e senha
        public function efetuarLogin($_login,$_senha)
        {
            $sql = new Sql();
            $senha_cript = md5($_senha);
            $results = $sql->select('SELECT * FROM administrador where login = :login AND senha = :senha',array(':login' => $_login,':senha' => $senha_cript));
            if (count($results) > 0) {
                $this->setData($results[0]);
            }
        }
        //Define os atributos 
        public function setData($data)
        {
            $this->setId($data['id']);
            $this->setNome($data['nome']);
            $this->setEmail($data['email']);
            $this->setLogin($data['login']);
            $this->setSenha($data['senha']);
        }
        //Insere administrador e retorna o adm
        public function insert()
        {
            $sql = new Sql();
            $results = $sql->select('CALL sp_adm_insert(:nome,:email,:login,:senha)',
            array(':nome'=>$this->getNome(),
            ':email'=>$this->getEmail(),
            ':login'=>$this->getLogin(),
            ':senha'=>md5($this->getSenha())));
            
            if (count($results) > 0) 
            {
                $this->setData($results[0]);
            }            
        }
        //Atualiza informações do adm            
        public function update($_id,$_nome,$_email,$_login)
        {   
            $sql = new Sql();
            $sql->query('UPDATE administrador set nome = :nome, email = :email, login = :login where id = :id',
            array(':id'=>$_id,':nome'=>$_nome,':email'=>$_email,':login'=>$_login));
        }
        //Deleta administrador
        public function delete($_id)
        {            
            $sql = new Sql();
            $sql->query('DELETE FROM administrador WHERE id = :id',array(':id'=>$_id));
        }
        //Recuperando senha
        public function recuperarSenha($destino,$assunto,$mensagem)
        {
            return mail($destino,$assunto,$mensagem,'From: dinamico85.com');
        }
        //Método construtor cheio e vazio
        public function __construct($_id='',$_nome="",$_email="",$_login="",$_senha="")
        {
            $this->id = $_id;
            $this->nome = $_nome;
            $this->email = $_email;
            $this->login = $_login;
            $this->senha = $_senha;
        }
    }
?>