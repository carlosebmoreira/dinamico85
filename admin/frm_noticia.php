<div id="box-cadastro">
    <div id="formulario-menor">
        <form action="op_noticia.php" method="post" enctype="multipart/form-data">
            <fieldset>  
                <legend>Inserir Noticia</legend>           
                <?php 
                    require_once('../config.php');
                    $cats = Categoria::listar();
                ?>
                <label for="">
                    <select name="id_categoria" id="id_categoria" style="width: 100%; height: 30px; font-size: 15pt">            
                        <?php 
                            foreach ($cats as $cat) 
                            {
                                echo "<option value=".$cat['id_categoria'].">".$cat['categoria']."</option>";
                            }
                        ?>            
                    </select>
                </label>
                <label for="">
                    Titulo
                    <input type="text" name="titulo_noticia">
                </label>
                <label for="">
                    img
                    <input type="file" name="img_noticia">
                </label>                
                <label for="">
                    Data
                    <input type="date" name="data_noticia">
                </label>
                <label for="">
                    Ativo
                    <input type="checkbox" name="ativo_noticia" checked>
                </label>
                <label for="">
                    Noticia
                    <input type="text" name="text_noticia">
                </label>
                <br>
                <label for="">                
                    <input type="submit" value="Inserir" name="btn_inserir_noticia">
                </label>
            </fieldset>
        </form>
    </div>
</div>