<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">    
    <title>Alteração de Categoria</title>
</head>
<body>    
    <form action="op_categoria.php?update=1" method="POST" enctype="multipart/form-data">
        <fieldset>
            <legend>Alteração do Categoria</legend>
            <div>
                <input type="hidden" name="id_cat" value="<?php echo filter_input(INPUT_GET,'id_categoria');?>">
            </div>
            <div>
                <label for="">Categoria</label>
                <input type="text" name="categoria" value="<?php echo filter_input(INPUT_GET,'categoria');?>">
            </div>
            <div>
                <label for="">Ativo</label>
                <input type="checkbox" name="ativo" <?php echo filter_input(INPUT_GET,'ativo')==1?'checked':''?>>
            </div>                
            <div>                
                <input type="submit" name="btn_alterar_categoria" value="Alterar Categoria">
            </div>
        </fieldset>
    </form>    
</body>
</html>
