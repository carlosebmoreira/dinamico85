<?php
    require_once('config.php');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <title>Site Dinamico UC12</title>
    <link rel="stylesheet" href="css/style.php">
</head>
<body>
    <div id="estrutura">
        <div id="topo">
        </div>
        <div id="menu">
            <ul>
               <li><a href="index.php?link=1">Home</a></li>
               <li><a href="index.php?link=">Serviços</a></li>
               <li><a href="index.php?link=">Produtos</a></li>
               <li><a href="index.php?link=">Contatos</a></li>
               <li>
                <?php
                    if(isset($_SESSION['logado_usuario']))
                    {
                        if($_SESSION['logado_usuario'])
                        {
                            echo '<a href="index.php?link=8">Perfil</a>';
                        }
                        else
                        {
                            echo '<a href="index.php?link=6">Logar-se</a>';
                        }
                    }
                    else
                    {
                        echo '<a href="index.php?link=6">Logar-se</a>';
                    }
                ?>
                </li> 
            </ul>
        </div>  
        <div id="banner" class="banner"> 
        </div>
        <div id="corpo">
            <div id="esquerda" class="esquerda">
                <h1>Produtos</h1>
                <li><a href="index.php?link=5">Produto 1</a></li>
                <li><a href="index.php?link=">Produto 2</a></li>
                <li><a href="index.php?link=">Produto 3</a></li>
                <li><a href="index.php?link=">Produto 4</a></li>
            </div>   
            <div id="centro" class="centro">
                <?php      
                    $_SESSION['idnot'] = filter_input(INPUT_GET,'idnoticia');
                    $_SESSION['idpost'] = filter_input(INPUT_GET,'id_post');
                    $link = isset($_GET['link'])? $_GET['link']:'';
                    $pag[1] = "home.php";                    
                    $pag[3] = "conteudo_noticia.php";
                    $pag[4] = "conteudo_post.php";
                    $pag[5] = "produto.php"; 
                    $pag[6] = "logar_user.php";
                    $pag[7] = "registrar_user.php";
                    $pag[8] = "frm_user.php";
                    if(!empty($link))
                    {
                        if(file_exists($pag[$link]))
                        {
                            include($pag[$link]);
                        }
                        else
                        {
                            include($pag[1]); //Mostre o home
                        }
                    }
                    else
                    {
                        include($pag[1]); //Mostre o home
                    }   
                ?>
            </div> 
            <div id="direita">
                <div id="noticias"> 
                    <span><h3>Noticias</h3></span>                    
                    <?php                        
                        $noticias = Noticia::listarNoticias();
                        foreach ($noticias as $noticia)
                        {   
                            if($noticia['noticia_ativo']=='s')
                            {
                    ?>   
                    <h3><img src="admin/foto/<?php echo $noticia['img_noticia']; ?>" alt="" width="90" height="80"></h3>
                    <div id="itens-noticias">
                        <span><?php echo $noticia['data_noticia']; ?></span>
                        <a href="index.php?link=3&idnoticia=<?php echo $noticia['id_noticia']; ?>"><?php echo $noticia['titulo_noticia']; ?></a>
                    </div>
                    <?php } } ?>                    
                </div>
                <div>
                    <br>
                    <hr>
                    <span><h3>Posts</h3></span> <br>
                    <?php
                        $pos = new Post();
                        $posts = $pos->listarPost();
                        foreach($posts as $post)
                        {
                            if($post['post_ativo']=='s')
                            {                    
                    ?>                    
                    <span><?php echo $post['data_post'];?></span><br>
                    <span><a href="index.php?link=4&id_post=<?php echo $post['id_post'];?>"><?php echo $post['titulo_post']?></a></span><br>
                    <h3><img src="admin/foto/<?php echo $post['img_post'];?>" alt="Imagem do post" width="90" height="80"></h3>
                    <?php
                                echo '<hr>';
                            }
                        }
                    ?>
                </div>
            </div>
            <div> 
                Área do administrador                   
                <a href="admin/index.php">Acesso à área administrativa</a>
            </div>
            <div id="rodape">
                &copy; - Todos os direitos reservados
            </div>                                
        </div>        
    </div>
</body>
</html>