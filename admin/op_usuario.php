<?php
    require_once('../config.php');    
    $user = new Usuario();
    #logando Usuario
    if(isset($_POST['btn_logar_usuario']))
    {
        if(isset($_POST['email_usuario']))        
        {            
            $user->logarUser($_POST['email_usuario'],$_POST['senha_usuario']);
            if($user->getId()>0)
            {
                $_SESSION['logado_usuario'] = true;
                $_SESSION['id_usuario'] = $user->getId();
                $_SESSION['nome_usuario'] = $user->getNome();
                $_SESSION['email_usuario'] = $user->getEmail();   
                $_SESSION['foto_usuario']  = $user->getFoto();
                var_dump($_SESSION);
                header('location:../index.php?msg=logado com Sucesso');
            }
            else
            {
                header('location:../index.php?link=6&msg=Email ou senha Incorreto!');
            }            
        }
    }
    #Cadastrando Usuario
    if(isset($_POST['btn_cadastrar_usuario']))
    {
        if(isset($_POST['nome_usuario']))
        {            
            $img = upload_imagem_user();
            $user->insertUser($_POST['nome_usuario'],$_POST['email_usuario'],$_POST['senha_usuario'],$img[0]);
            if($user->getId()>0)
            {
                header('location:../index.php?link=7&msg=Cadastrado com Sucesso');                
            }
            else
            {
                header('location:../index.php?link=7&msg=Erro ao cadastrar-se');                
            }
        }
    }
    #Encerrando a sessão
    if(isset($_GET['sair']))
    {
        if($_GET['sair']==1)
        {
            $_SESSION['logado_usuario'] = null;
            $_SESSION['id_usuario'] = null;
            $_SESSION['nome_usuario'] = null;
            $_SESSION['email_usuario'] = null;                
            var_dump($_SESSION);
            header('location:../index.php');
        }
    }
?>