<?php
    include_once('../config.php');    
    if(isset($_SESSION['logado']))
    {
        if($_SESSION['logado'])
        {
            header('location:principal.php');
        }
    }
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <title>Site Dinâmico - Área administrativa</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div id="box-login">
        <div id="formulario-login">
            <form id="frm_login" action="op_administrador.php" method="post" name="frm_login">
                <fieldset>
                    <legend>Faça Login - Área Administrativa</legend>
                    <label for=""><span>Login</span></label>
                    <input type="text" name="txt_login" id="txt_login">

                    <label for=""><span>Senha</span></label>
                    <input type="password" name="txt_senha" id="txt_senha">
                    <input type="submit" name="btn_logar_user" id="logar" value="Logar" class="botao">                    
                    <span><?php echo isset($_GET['msg'])?$_GET['msg']:'';?></span>
                </fieldset>
            </form>
            <input type="hidden" name="btn_recuperar_senha" id="recuperar_senha" value="Recuperar Senha" class="botao" onclick="recuperarSenha()">
        </div>
    </div>
</body>
</html>
<script>
    function recuperarSenha()
    {
        location.href = 'recuperar_senha.php'
    }    
</script>