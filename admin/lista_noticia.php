<?php
    require_once('../config.php');            
    $noticia = new Noticia();
    
    if(count($nots = $noticia->listarNoticiasInner()) > 0)
    {
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <title>Lista Noticias</title>
</head>
<body>
    <table width='100%' border="" cellpadding="0" cellspacing="1" bgcolor="">
        <tr bgcolor="#993300" align="center">
            <th width="5%" height="2" align="rigth"><font size="2" color="#fff">ID</font></th>
            <th width="15%" height="2" align="rigth"><font size="2" color="#fff">Categoria</font></th>
            <th width="20%" height="2" align="rigth"><font size="2" color="#fff">Titulo</font></th>
            <th width="20%" height="2" align="rigth"><font size="2" color="#fff">Img</font></th>
            <th width="5%" height="2" align="rigth"><font size="2" color="#fff">Visitas</font></th>
            <th width="15%" height="2" align="rigth"><font size="2" color="#fff">Data</font></th>
            <th width="5%" height="2" align="rigth"><font size="2" color="#fff">Ativo</font></th>
            <th width="20%" height="2" align="rigth"><font size="2" color="#fff">Noticia</font></th>
            <th colspan="2" align="center"><font size="2" color="#fff">Opções</font></th>
        </tr>
        <?php            
            foreach($nots as $not)
            {
        ?>
        <tr>
            <td><?php echo $not['id_noticia']?></td>
            <td><?php echo $not['categoria']?></td>
            <td><?php echo $not['titulo_noticia']?></td>
            <td><img src="foto/<?php echo $not['img_noticia']?>" alt="" width="150" height="150"></td>
            <td><?php echo $not['visita_noticia']?></td>
            <td><?php echo $not['data_noticia']?></td>
            <td><?php echo $not['noticia_ativo']=='s'?'Sim':'Não';?></td>
            <td><?php echo $not['noticia']?></td>
            <td align="center"><font size="2" face="verdana, arial" color="#000">
                <a href="<?php echo 'alterar_noticia.php?id_noticia='.$not['id_noticia'].'&nome_categoria='.$not['categoria'].
                '&titulo='.$not['titulo_noticia'].'&img='.$not['img_noticia'].'&visita='.$not['visita_noticia'].'&data='.$not['data_noticia'].
                '&ativo='.$not['noticia_ativo'].'&noticia='.$not['noticia']?>">Alterar</a>
            </font></td>
            <td align="center"><font size="2" face="verdana, arial" color="#000">
                <a href="<?php echo 'op_noticia.php?excluir=1&id_noticia='.$not['id_noticia']?>">Excluir</a>
            </font></td>
        </tr>
        <?php
            }
        }
        ?>
    </table>
</body>
</html>